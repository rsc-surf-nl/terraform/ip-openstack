output "id" {
  value = openstack_networking_floatingip_v2.floatip.id
}

output "ip_id" {
  value = openstack_networking_floatingip_v2.floatip.id
}

output "address" {
  value = openstack_networking_floatingip_v2.floatip.address
}

output "description" {
  value = var.description
}


output "router_external_network_id" {
  value = var.router_external_network_id
}
