resource "openstack_networking_floatingip_v2" "floatip" {
  pool        = var.floating_ip_pool
  description = var.description
  tags = [
    var.workspace_id,
    var.subscription,
    var.application_type,
    var.resource_type,
    var.cloud_type,
    var.subscription_group,
  ]
}
